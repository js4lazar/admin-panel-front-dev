
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.users', {
            abstract: false,
            url: '/admin-panel/usuarios',
            template: require('./users/list/userList.html'),
            controller: 'userListController',
            controllerAs:'userList'
      })
     .state('app.userCreate', {
            abstract: false,
            url: '/admin-panel/usuarios/crear',
            template: require('./users/create/userCreate.html'),
            controller: 'userCreateController',
            controllerAs:'userCreate'
      })
    .state('app.userEdit', {
            abstract: false,
            url: '/admin-panel/usuarios/edit?id',
            template: require('./users/edit/userEdit.html'),
            controller: 'userEditController',
            controllerAs:'userEdit'
      })
     .state('app.roles', {
            abstract: false,
            url: '/admin-panel/roles',
            template: require('./roles/list/rolesList.html'),
            controller: 'rolesListController',
            controllerAs:'rolesList'
      })
     .state('app.rolesCreate', {
            abstract: false,
            url: '/admin-panel/roles/crear',
            template: require('./roles/create/rolesCreate.html'),
            controller: 'rolesCreateController',
            controllerAs:'rolesCreate'
      })
     .state('app.groups', {
            abstract: false,
            url: '/admin-panel/grupos',
            template: require('./groups/list/groupsList.html'),
            controller: 'groupsListController',
            controllerAs:'groupsList'
      })
     .state('app.groupsCreate', {
            abstract: false,
            url: '/admin-panel/grupos/crear',
            template: require('./groups/create/groupsCreate.html'),
            controller: 'groupsCreateController',
            controllerAs:'groupsCreate'
      });
     
 
}
