/**
* @name APP cmmApp
* @autor camila.montenegro@u-planner.com
* @description
* # name controller --> rolesCreateController
* # As Controller --> rolesCreate
* brief comment controller creation roles users
*/

	export default class rolesCreateController {

		constructor($scope,rolesServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location) {

            var vm = this;
            this.$scope = $scope;
            this.ngNotify = ngNotify;
            this.$state = $state ;
            vm.services   = rolesServices.services;
            vm.save = save;
            $scope.name = '';
            $scope.grid = true;
           
            //function devuelve id user

            $timeout(function(){
                $scope.userinfo = UserInfoConstant[0].details;
                console.log('id usuario', $scope.userinfo[0]);
                vm.user=$scope.userinfo[0].user.id;
                vm.USER_ID=$scope.userinfo[0].user.id;
            },500);


            this.detailsRoles ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'mname',
                    displayName: 'Modulo',
                    // width: '210'
                    },
                    {
                    field: 'fname',
                    displayName: 'Caracteristicas', 
                     // width: '460'
                    },
                    {
                    field: 'ds_permissions',
                    displayName: 'Permisos',
                     // width: '460'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    // width: '110',
                    enableSorting: false,
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" ng-click="grid.appScope.rolesCreate.selectAllItems(checker.checked)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="true" ng-false-value="false" > \
                                    <i class="green"></i></label></span></div>'

                    }
                    ]
            };

            vm.init()

            function save(){

                $scope.isDisabled = true;

                var arrayRoles=[];
                angular.forEach(this.detailsRoles.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayRoles.push({fid:value.fid,mid:value.mid,permission:value.ds_permissions}) 
                    }
                });

                var rolesList=arrayRoles;
                let params  = { 
                    name   : $scope.name,
                    user_id : vm.USER_ID,
                    roles   : rolesList 
                    
                }

                vm.saveRoles(params)
            }

   }//Fin constructor

   //functions
 
    init(){

          this.getDetailsRolesFunction();
            
    }

    selectAllItems(value){
                this.detailsRoles.data.forEach(function(item){
                        item.hasPermission=value;
                })
            }

    getDetailsRolesFunction(){

                this.services.getDetailsRoles().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.detailsRoles.data = dataReturn.data;
                        console.log('details Roles',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

    }

    saveRoles(params){
                console.log('data envia servicio save',params);

                if(params.roles.length > 0){
                this.services.saveRoles(params).$promise.then((dataReturn) => {
                    if(dataReturn.status == 0){


                        this.idRole = dataReturn.data;

                        let dataRoles = {
                             roleid:this.idRole ,
                             values: params.roles
                            }

                        this.services.insertRolePermission(dataRoles).$promise.then((dataReturn) => {
                            console.log('se han asociado los permisos correctamente a al rol creado');
                        })

                        this.ngNotify.set('Se ha creado el Rol correctamente','success');
                        this.$state.go("app.roles");

                    }else{
                        this.ngNotify.set('Error con servicio check request','error');
                        this.$scope.isDisabled = false;
                    
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                    this.$scope.isDisabled = false;
                })
        }else{
            this.ngNotify.set('Error , Seleccione permisos para nuevo Rol','error');
            this.$scope.isDisabled = false;

        }

    }

}// fin function principal
rolesCreateController.$inject=['$scope', 'rolesServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location']





