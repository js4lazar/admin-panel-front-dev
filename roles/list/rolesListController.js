/**
* @name APP cmmApp
* @autor camila.montenegro@u-planner.com
* @description
* # name controller --> rolesListController
* # As Controller --> rolesList
* brief comment controller shows the view for the main page of assignTimeRange
*/

	export default class rolesListController {

		constructor($scope,rolesServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location) {

            var vm = this;
            this.$scope = $scope;
            this.ngNotify = ngNotify;
            this.$state = $state;
            vm.services   = rolesServices.services;
            vm.modalDelete = modalDelete;
            vm.editRolesSave = editRolesSave;
            $scope.grid = true;
            this.$scope.edit = true;
            this.$scope.titleList = true;
           
            //function devuelve id user

            $timeout(function(){
                $scope.userinfo = UserInfoConstant[0].details;
                console.log('id usuario', $scope.userinfo[0]);
                vm.user=$scope.userinfo[0].user.id;
                vm.USER_ID=$scope.userinfo[0].user.id;
            },500);


            this.detailsRoles ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'mname',
                    displayName: 'Modulo',
                    width: '180'
                    },
                    {
                    field: 'fname',
                    displayName: 'Caracteristicas', 
                     width: '380'
                    },
                    {
                    field: 'ds_permissions',
                    displayName: 'Permisos',
                     width: '380'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    width: '90',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="true" ng-false-value="false" ng-click="grid.appScope.rolesList.selectAllItems(checker.checked)" ng-disabled="(grid.appScope.edit == true)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="true" ng-false-value="false" ng-disabled="(grid.appScope.edit == true)"> \
                                    <i class="green"></i></label></span></div>'

                    }
                    ]
            };

            vm.init()

            function modalDelete(){
                    
                    $scope.userinfo = UserInfoConstant[0].details;
                    $scope.roleid = this.roleid;
                    $scope.rolename= this.rolename;
                    var modalInstance  = $modal.open({
                    template: require('../modal-delete/modalDeleteRoles.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'modalDeleteRolesController',
                    controllerAs: 'modalDeleteRoles'

                });
            
            }

            function editRolesSave(){

                $scope.isDisabled = true;

                var arrayRoles=[];
                angular.forEach(this.detailsRoles.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayRoles.push({fid:value.fid,mid:value.mid,permission:value.ds_permissions}) 
                    }
                });

                var rolesList=arrayRoles;
                let params  = { 
                    id   : this.roleid,
                    roles   : rolesList 
                    
                }

                vm.saveEditRoles(params)
            }

   }//Fin constructor

   //functions
            
    loadRoles(item){

        this.params = {

            id :item.roleid
        }

        this.roleid=item.roleid;
        this.rolename=item.rolename;
        this.getDetailsRolesFunction(this.params);
    }


    init(){

                this.services.getRoles().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                         this.ngNotify.set('no existen registros','warn');

                    }else{
                        
                        this.roles = dataReturn.data;
                        this.roleid=this.roles[0].roleid;
                        this.rolename=this.roles[0].rolename;
                        this.params = {
                            id : this.roles[0].roleid
                        }
                        this.getDetailsRolesFunction(this.params);
                    }
                }, (err) => {
                     this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

    }

    selectAllItems(value){

                this.detailsRoles.data.forEach(function(item){
                        item.hasPermission=value;
                })
            }

    getDetailsRolesFunction(params){

                this.services.getDetailsRoles(params).$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.detailsRoles.data = dataReturn.data;
                        console.log('details Roles',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

    editRoles(){

        this.$scope.editSave = true;
        this.$scope.edit = false;
        this.$scope.titleList = false;
        this.$scope.titleEdit = true;
        this.$scope.isDisabled = false;
   
    }

    cancelEditRoles(){

        this.$scope.editSave = false;
        this.$scope.edit = true;
        this.$scope.titleList = true;
        this.$scope.titleEdit = false;
        this.getDetailsRolesFunction({id : this.roleid});
   
    }

    saveEditRoles(params){

            console.log('esto es lo que se envia',params);

                let updateRoles = {
                         roleid:params.id ,
                         values: params.roles
                        }

                this.services.insertRolePermission(updateRoles).$promise.then((dataReturn) => {
                    if(dataReturn.status == 0){
                        this.ngNotify.set('Se ha modificado de manera correcta el rol','success');
                        // this.$state.go(this.$state.current, {}, {reload: true});
                        this.$scope.edit = true;
                        this.$scope.editSave = false;
                        this.$scope.titleList = true;
                        this.$scope.titleEdit = false;

                    }else{
                        console.log('Error Servicio update check params enviados',dataRoles);
                        this.$scope.isDisabled = false;
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                    this.$scope.isDisabled = false;
                })

    }



}// fin function principal
rolesListController.$inject=['$scope', 'rolesServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location']





