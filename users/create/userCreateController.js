/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userCreateController
* # As Controller --> userCreate
* brief comment controller the view for the list user active admin-Panel
*/

	export default class userCreateController {

		constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state) {


			var vm = this;
			this.ngNotify = ngNotify;
			this.$scope = $scope;
			this.$state = $state;
			vm.services   = userServices.services;
			vm.save = save;
			$scope.name = '';
			$scope.email = '';
			$scope.username = '';
			$scope.status =  true;
			$scope.institution = 0;
			this.$scope.afterSave = true;
			$scope.password = '';
			vm.rolesList=[];
			vm.groupsList=[];
			// $scope.notPicture= require("../images/no_user.jpg");
			$scope.picture= require("../images/no_user.jpg");
      	
	   		
			vm.init();

	   		function save(){

	   			$scope.isDisabled = true;
        
	   			let params  = { 
					name   : $scope.name ,
					email   : $scope.email ,
					picture : $scope.file,
                    username : $scope.username,
                    institution   : $scope.institution,
                    status : $scope.status, 
                    password : $scope.password,
                    roles : vm.rolesList,
                    groups:vm.groupsList,
                }

	   			vm.saveUser(params)
	   		}

			}//fin constructor

			//functions

			init(){

				this.getCataloguesRoles();
				this.getCataloguesGroups();
				this.getCataloguesInstitutions();


			}

			getCataloguesRoles(){

                this.services.getRoles().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.roles = dataReturn.data;
                        console.log('Roles',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

            getCataloguesGroups(){

                this.services.getGroups().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.groups = dataReturn.data;
                        console.log('Groups',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

            getCataloguesInstitutions(){

                this.services.getInstitutions().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.institutions = dataReturn.data;
                        console.log('institutions',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }
			
			saveUser(params){
				console.log('data envia servicio save',params);

				let dataValidate  = { 
					email   : params.email,
                    name : params.username
            }


				this.services.validateUser(dataValidate).$promise.then((dataReturn) => {
					if(dataReturn.status == 1){
						this.ngNotify.set('El usuario ingresado ya existe , intentelo usando otro correo y usuario','error');


					}else{
							this.services.saveUsers(params).$promise.then((dataReturn) => {
								if(dataReturn.status == 0){
									
									this.ngNotify.set('Se ha creado el usuario correctamente','success');
									this.$state.go("app.users");

								}else{
									this.ngNotify.set('Error , con el servicio','error');
									this.$scope.isDisabled = false;
								
								}
							}, (err) => {
								this.ngNotify.set('No se ha podido conectar con el servicio','error');
								this.$scope.isDisabled = false;
							})
					
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
					this.$scope.isDisabled = false;
				})

				

			}

  }

userCreateController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state']
