/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userListController
* # As Controller --> userList
* brief comment controller the view for the list user active admin-Panel
*/

	export default class userListController {

		constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state,$modal) {


			var vm = this;
			this.ngNotify = ngNotify;
			this.$scope = $scope;
			vm.services   = userServices.services;
			vm.modalDelete  = modalDelete;
			vm.userEdit =userEdit;
			// $scope.notPicture= require("../images/no_user.jpg");
			$scope.picture= require("../images/no_user.jpg");
      	
	   		vm.init()

	   		function modalDelete(){
	   				
	   				$scope.userinfo = UserInfoConstant[0].details;
	   				$scope.idUser = this.id;
	   				$scope.nameUser= this.name;
					var modalInstance  = $modal.open({
		            template: require('../modal-delete/modalDeleteUser.html'),
		            animation: true,
		            scope: $scope,
		            controller: 'modalDeleteUserController',
		            controllerAs: 'modalDeleteUser'

		        });
			
	   		}
	   		function userEdit(id){
               $state.go('app.userEdit', {id:id});
	   		}

			}//fin constructor

			//functions
			
			loadUser(item){

				this.params = {

					id :item.id
				}

				this.name=item.name;
				this.username=item.username;
				this.id=item.id;
				this.getDetailsUserFunction(this.params);
			}

			
			init(params){

				this.services.getDataUsers(params).$promise.then((dataReturn) => {
					if(dataReturn.data.lenght == 0){
						this.ngNotify.set('registro seleccionado no tiene registros','warn');

					}else{
						
						this.users = dataReturn.data;
	                    this.username=this.users[0].username;
						this.name=this.users[0].name;
						this.id=this.users[0].id;
	                    this.params = {
						    id : this.users[0].id
					    }
						this.getDetailsUserFunction(this.params);
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
				})

			}

			
			getDetailsUserFunction(params){

				this.services.getDetailsUser(params).$promise.then((dataReturn) => {
					if(dataReturn.data.lenght == 0){
						this.ngNotify.set('registro seleccionado no tiene registros','warn');

					}else{
						
						this.detailsUser = dataReturn.data;
						console.log('details users',dataReturn.data);
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
				})

			}

			

  }

userListController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state','$modal']
