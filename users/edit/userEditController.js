/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userCreateController
* # As Controller --> userCreate
* brief comment controller the view for the list user active admin-Panel
*/

	export default class userEditController {

		constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state,$location) {


			var vm = this;
			this.ngNotify = ngNotify;
			this.$scope = $scope;
			this.$state = $state;
			vm.services   = userServices.services;
			vm.save = save;
			$scope.name = '';
			$scope.email = '';
			$scope.username = '';
			$scope.status =  true;
			$scope.institution = 0;
			$scope.password = '';
			vm.rolesList=[];
			vm.groupsList=[];
			// $scope.notPicture= require("../images/no_user.jpg");
			$scope.picture= require("../images/no_user.jpg");
      	    vm.id=$location.search().id;
      	    vm.oldname='';
	   		
			vm.init();

	   		function save(){

	   			$scope.isDisabled = true;

	   		    var institution='';
	   			if(typeof vm.institution === 'undefined')
	   			   var institution=$('#institution').val();
	   			else
	   				institution= vm.institution;

	   
	   			let params  = { 
	   				username : vm.username,
					name   : vm.name ,
					email   : vm.email ,
					picture : vm.file,
                    username : vm.username,
                    institution   : institution,
                    status : vm.status, 
                    password : vm.password,
                    roles : vm.rolesList,
                    groups:vm.groupsList,
                    oldname:vm.oldname,
                    user_id:vm.id
                }

	   			vm.saveUser(params)
	   		}

			}//fin constructor

			//functions

			init(){
               
                this.getCataloguesInstitutions();
				this.getCataloguesRoles();
				this.getCataloguesGroups();
				this.getDetailsUserFunction({id:this.id});
               // console.log('....',this.id);
				


			}


			getDetailsUserFunction(params){

			this.services.getDetailsUser(params).$promise.then((dataReturn) => {
				if(dataReturn.data.lenght == 0){
					this.ngNotify.set('registro seleccionado no tiene registros','warn');

				}else{
					
					this.detailsUser = dataReturn.data;
                    this.name=this.detailsUser.fullname;
                    this.username=this.detailsUser.username;
                    this.password=this.detailsUser.prim_keys;
                    this.password2=this.detailsUser.prim_keys;
					this.email=this.detailsUser.mail;
					this.oldname=this.detailsUser.username;
				
					// this.file,
					if(this.detailsUser.is_active=='Active')
					   this.status=true;
					else
					   this.status=false;

                      
                       $('#institution').val(this.detailsUser.institutionID);
                       // this.institution=this.detailsUser.institutionID;
                       this.groupsList=this.detailsUser.groups;
                       this.rolesList=this.detailsUser.roles;
                       // this.groupname=['Servicios','developers'];
                    
                    // this.rolesList,
                    // this.groupsList,

					console.log('details users',dataReturn.data);
				}
			}, (err) => {
				this.ngNotify.set('No se ha podido conectar con el servicio','error');
			})

			}

			getCataloguesRoles(){

                this.services.getRoles().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.roles = dataReturn.data;
                        console.log('Roles',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

            getCataloguesGroups(){

                this.services.getGroups().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.groups = dataReturn.data;
                        console.log('Groups',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

            getCataloguesInstitutions(){

                this.services.getInstitutions().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros','warn');

                    }else{
                        
                        this.institutions = dataReturn.data;
                        console.log('institutions',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }
			
			saveUser(params){
				console.log('data envia servicio save',params);

				this.services.updateUser(params).$promise.then((dataReturn) => {
					if(dataReturn.status == 0){
						this.ngNotify.set('Usuario editado correctamente','success');
						this.$state.go("app.users");

					}else{
						this.ngNotify.set('Error ,Usuario editado sin éxito','error');
						this.$scope.isDisabled = false;
					
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
					this.$scope.isDisabled = false;
				})

			}

			// saveUser(params){
			// 	console.log('data envia servicio edit',params);

			// 	let dataValidate  = { 
			// 		email   : params.email ,
   			//      name : params.username
   			//   }


			// 	this.services.validateUser(dataValidate).$promise.then((dataReturn) => {
			// 		if(dataReturn.status == 1){
			// 			this.ngNotify.set('El usuario ingresado ya existe , intentelo otra usando otro correo y userio','error');


			// 		}else{
			// 				this.services.updateUser(params).$promise.then((dataReturn) => {
			// 					if(dataReturn.status == 0){
			// 						this.ngNotify.set('Usuario editado correctamente','success');
			// 						this.$state.go("app.users");

			// 					}else{
			// 						this.ngNotify.set('Error , con el servicio','error');
								
			// 					}
			// 				}, (err) => {
			// 					this.ngNotify.set('No se ha podido conectar con el servicio','error');
			// 				})
					
			// 		}
			// 	}, (err) => {
			// 		this.ngNotify.set('No se ha podido conectar con el servicio','error');
			// 	})

				

			// }

  }

userEditController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state','$location']
