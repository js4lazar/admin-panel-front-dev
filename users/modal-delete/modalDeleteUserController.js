/**
* @name APP cmmApp
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> modalDeleteUserController
* # As Controller --> modalDeleteUser
* controllador de modal para eliminar usuario 
*/


  export default class modalDeleteUserController {

  constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state,$modalInstance){

    
      var vm = this;
      vm.deleteUser = deleteUser;
      this.ngNotify = ngNotify ;
      this.$state = $state;
      vm.cancel = cancel;
      this.$modalInstance = $modalInstance;
      vm.services   = userServices.services;
      vm.id = $scope.idUser;
      vm.name = $scope.nameUser;
      vm.user=$scope.userinfo[0].user.id;
     
    // /////////////////////////////

    function deleteUser() {

          var params = {

            status:false,
            id_code : vm.id

          }

        vm.deleteUserFunction(params)
         
        } 


    function cancel() {

          $modalInstance.dismiss('chao');
        }

    }//fin constructor

  
      deleteUserFunction(params){
                console.log('data envia servicio save',params);

                this.services.deleteUser(params).$promise.then((dataReturn) => {
                    if(dataReturn.status == 0){
                        this.ngNotify.set('El usuario ha sido desactivado de manera correcta','success');
                        this.$modalInstance.dismiss('chao');
                        this.$state.go(this.$state.current, {}, {reload: true});

                    }else{
                        this.ngNotify.set('Error ,problemas con el servicio no se ha podidio eliminar el usuario','error');
                    
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

  }

  modalDeleteUserController.$inject = ['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state','$modalInstance'];
