
// file Route & run app adminPanel
import routing from './adminPanel.route';
import run from './adminPanel.run';

// controllers & services module Users
import userListController from './users/list/userListController';
import userCreateController from './users/create/userCreateController';
import userEditController from './users/edit/userEditController';
import modalDeleteUserController from './users/modal-delete/modalDeleteUserController';
import userServices from './users/userServices';

// controllers & services module Roles
import rolesListController from './roles/list/rolesListController';
import rolesCreateController from './roles/create/rolesCreateController';
import modalDeleteRolesController from './roles/modal-delete/modalDeleteRolesController';
import rolesServices from './roles/rolesServices';

// controllers & services module groups
import groupsListController from './groups/list/groupsListController';
import groupsCreateController from './groups/create/groupsCreateController';
import modalDeleteGroupsController from './groups/modal-delete/modalDeleteGroupsController';
import groupsServices from './groups/groupsServices';

export default angular.module('app.adminPanel', [userServices,rolesServices,groupsServices])
  .config(routing)
  .controller('userListController', userListController)
  .controller('userCreateController', userCreateController)
  .controller('userEditController', userEditController)
  .controller('modalDeleteUserController', modalDeleteUserController)
  .controller('rolesListController', rolesListController)
  .controller('rolesCreateController', rolesCreateController)
  .controller('modalDeleteRolesController', modalDeleteRolesController)
  .controller('groupsListController', groupsListController)
  .controller('groupsCreateController', groupsCreateController)
  .controller('modalDeleteGroupsController', modalDeleteGroupsController)
  .run(run)
  .constant("adminPanel", [{

        "title": "Admin-Panel",
        "icon":"mdi-action-perm-contact-cal",
        "subMenu":[
            {
            'title' : 'Administración Usuarios',
            'url': 'app.users',
            },{
            'title':'Administración Roles',
            'url': "app.roles",
            },{
            'title':'Administración Grupos',
            'url': "app.groups",
            }
        ]
    }])
  .name;


