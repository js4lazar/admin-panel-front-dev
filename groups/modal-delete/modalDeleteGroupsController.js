
/**
* @name APP cmmApp
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> modalDeleteGroupsController
* # As Controller --> modalDeleteGroups
* controllador de modal para eliminar grupo 
*/


  export default class modalDeleteGroupsController {

  constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,groupsServices,$state,$modalInstance){

    
      var vm = this;
      vm.deleteGroups = deleteGroups;
      this.ngNotify = ngNotify ;
      this.$state = $state;
      vm.cancel = cancel;
      this.$modalInstance = $modalInstance;
      vm.services   = groupsServices.services;
      vm.id = $scope.groupid;
      vm.name = $scope.groupname;
      vm.user=$scope.userinfo[0].user.id;
     
    // /////////////////////////////

    function deleteGroups() {

          var params = {

            status:false,
            id_code : vm.id

          }

        vm.deleteGroupsFunction(params)
         
        } 


    function cancel() {

          $modalInstance.dismiss('chao');
        }

    }//fin constructor

  
      deleteGroupsFunction(params){
                console.log('data envia servicio save',params);

                this.services.deleteGroups(params).$promise.then((dataReturn) => {
                    if(dataReturn.status == 0){
                        this.ngNotify.set('El grupo ha sido desactivado de manera correcta','success');
                        this.$modalInstance.dismiss('chao');
                        this.$state.go(this.$state.current, {}, {reload: true});

                    }else{
                        this.ngNotify.set('Error ,problemas con el servicio no se ha podidio eliminar el usuario','error');
                    
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

  }

  modalDeleteGroupsController.$inject = ['$scope','$resource','$timeout','ngNotify','UserInfoConstant','groupsServices','$state','$modalInstance'];

