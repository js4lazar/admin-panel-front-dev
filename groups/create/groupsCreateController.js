/**
* @name APP cmmApp
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> groupsCreateController
* # As Controller --> groupsCreate
* brief comment controller shows the main view groups-Create
*/

	export default class groupsCreateController {

		constructor($scope,groupsServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location) {

            var vm = this;
            this.$scope = $scope;
            this.ngNotify = ngNotify;
            this.$state = $state;
            vm.services   = groupsServices.services;
            vm.save = save;
            vm.showGridCareer = showGridCareer;
            vm.showGridCampus = showGridCampus;
            vm.showGridDepartament = showGridDepartament;
            $scope.name = '';
            $scope.gridCampus = true;

            //function devuelve id user

            $timeout(function(){
                $scope.userinfo = UserInfoConstant[0].details;
                console.log('id usuario', $scope.userinfo[0]);
                vm.user=$scope.userinfo[0].user.id;
                vm.USER_ID=$scope.userinfo[0].user.id;
            },500);


            this.detailsGroups ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institucionName',
                    displayName: 'Institucion',
                    width: '300'
                    },
                    {
                    field: 'campusName',
                    displayName: 'Campus', 
                    width: '400'
                    },
                    {
                    field: 'periodName',
                    displayName: 'Periodo',
                    width: '400'
                    },
                    {
                    field: 'jornadaName',
                    displayName: 'Jornada',
                     width: '210'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    width: '80',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsCreate.selectAllCampus(checker.checked)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            this.detailsGroupsCareer ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institution_name',
                    displayName: 'Institucion',
                    // width: '220'
                    },
                    {
                    field: 'faculty_name',
                    displayName: 'Facultad', 
                     // width: '450'
                    },
                    {
                    field: 'career_name',
                    displayName: 'Carrera',
                     // width: '450'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    // width: '90',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsCreate.selectAllCareer(checker.checked)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            this.detailsGroupsDepartament ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institution_name',
                    displayName: 'Institucion',
                    // width: '220'
                    },
                    {
                    field: 'unit_name',
                    displayName: 'Unidad Academica', 
                     // width: '450'
                    },
                    {
                    field: 'faculty_name',
                    displayName: 'Facultad',
                     // width: '450'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    // width: '90',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsCreate.selectAllDepartament(checker.checked)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            vm.init()

            function save(){

                $scope.isDisabled = true;

                var arrayValues=[];
                angular.forEach(this.detailsGroups.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_campus_academicperiod_shift',section_id:value.id_section}) 
                    }
                });

                angular.forEach(this.detailsGroupsCareer.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_careers',section_id:value.id_section}) 
                    }
                });

                angular.forEach(this.detailsGroupsDepartament.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_academicunits',section_id:value.id_section}) 
                    }
                });

             
                var values=arrayValues;

                let params  = { 
                    name   : $scope.name ,
                    user_id : vm.USER_ID,
                    values   : values
                    
                }

                vm.saveGroups(params)
            }

            function showGridCareer(){

                $scope.gridCampus = false;
                $scope.gridCareer = true;
                $scope.gridDepartament = false;
                
            }

            function showGridDepartament(){
                
                $scope.gridCampus = false;
                $scope.gridCareer = false;
                $scope.gridDepartament = true;       
            }

            function showGridCampus(){
                $scope.gridCampus = true;
                $scope.gridCareer = false;
                $scope.gridDepartament = false;
                
            }

   }//Fin constructor

   //functions
            
    init(){
  
            this.getDetailsGroupsFunction();
            this.getDetailsGroupsCareerFunction();
            this.getDetailsGroupsDepartamentFunction();
            this.$scope.gridCampus = true;
            this.$scope.gridCareer = false;
            this.$scope.gridDepartament = false;
              
    }

    selectAllCampus(value){
      
        this.detailsGroups.data.forEach(function(item){
                item.hasPermission=value;
        })
    }

    selectAllCareer(value){
      
        this.detailsGroupsCareer.data.forEach(function(item){
                item.hasPermission=value;
        })
    }
    selectAllDepartament(value){
      
        this.detailsGroupsDepartament.data.forEach(function(item){
                item.hasPermission=value;
        })
    }

    getDetailsGroupsFunction(){

                this.services.getAllPermissionsByCampusByGroup().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros Campus','warn');

                    }else{
                        
                        this.detailsGroups.data = dataReturn.data;
                        console.log('details groups',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

    }

    getDetailsGroupsCareerFunction(){

                this.services.getAllPermissionsByCareerByGroup().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros Carreras','warn');

                    }else{
                        
                        this.detailsGroupsCareer.data = dataReturn.data;
                        console.log('details groups',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

    }

    getDetailsGroupsDepartamentFunction(){

                this.services.getAllPermissionsByDepartmentByGroup().$promise.then((dataReturn) => {
                    if(dataReturn.data.lenght == 0){
                        this.ngNotify.set('registro seleccionado no tiene registros Departamentos','warn');

                    }else{
                        
                        this.detailsGroupsDepartament.data = dataReturn.data;
                        console.log('details groups',dataReturn.data);
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

    }

    saveGroups(params){
                console.log('data envia servicio save',params);

                if(params.values.length > 0){

                this.services.saveGroups(params).$promise.then((dataReturn) => {
                    if(dataReturn.status == 0){

                        this.Groups = dataReturn.data;

                        let dataGroups = {
                            groupid:this.Groups ,
                             values: params.values
                            }

                        this.services.saveGroupsPermissions(dataGroups).$promise.then((dataReturn) => {
                            console.log('se han asociado los permisos correctamente a al grupo creado');
                        })

                        this.ngNotify.set('Se ha creado el Grupo correctamente','success');
                        this.$state.go("app.groups");

                    }else{
                        this.ngNotify.set('Error con servicio, check request','error');
                        this.$scope.isDisabled = false;
                    
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                    this.$scope.isDisabled = false;
                })

            }else{

                this.ngNotify.set('Error , Seleccione Permisos para el nuevo Grupo','error');
                this.$scope.isDisabled = false;

            }

        }



}// fin function principal
groupsCreateController.$inject=['$scope', 'groupsServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location']





