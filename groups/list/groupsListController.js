/**
* @name APP cmmApp
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> groupsListController
* # As Controller --> groupsList
* brief comment controller shows the main view groupsList
*/

	export default class groupsListController {

		constructor($scope,groupsServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location) {

            var vm = this;
            this.$scope = $scope;
            this.ngNotify = ngNotify;
            vm.services   = groupsServices.services;
            vm.modalDelete   = modalDelete;
            vm.showGridCareer = showGridCareer;
            vm.showGridCampus = showGridCampus;
            vm.showGridDepartament = showGridDepartament;
            vm.editGroupsSave = editGroupsSave;
            this.$scope.edit = true;
            this.$scope.titleList = true;
            $scope.gridCampus = true;
            $scope.gridCareer = false;
            $scope.gridDepartament = false;
            //function devuelve id user

            $timeout(function(){
                $scope.userinfo = UserInfoConstant[0].details;
                console.log('id usuario', $scope.userinfo[0]);
                vm.user=$scope.userinfo[0].user.id;
                vm.USER_ID=$scope.userinfo[0].user.id;
            },500);


            this.detailsGroups ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institucionName',
                    displayName: 'Institucion',
                    // width: '220'
                    },
                    {
                    field: 'campusName',
                    displayName: 'Campus', 
                     // width: '410'
                    },
                    {
                    field: 'periodName',
                    displayName: 'Periodo',
                     // width: '220'
                    },
                    {
                    field: 'jornadaName',
                    displayName: 'Jornada',
                     // width: '220'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    // width: '120',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsList.selectAllCampus(checker.checked)" ng-disabled="(grid.appScope.edit == true)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0" ng-disabled="(grid.appScope.edit == true)"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            this.detailsGroupsCareer ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institution_name',
                    displayName: 'Institucion',
                    // width: '230'
                    },
                    {
                    field: 'faculty_name',
                    displayName: 'Unidad Academica', 
                     // width: '380'
                    },
                    {
                    field: 'career_name',
                    displayName: 'Carrera',
                     // width: '310'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    // width: '90',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsList.selectAllCareer(checker.checked)" ng-disabled="(grid.appScope.edit == true)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0" ng-disabled="(grid.appScope.edit == true)"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            this.detailsGroupsDepartament ={
                
                enableFiltering: true,
                // data:$scope.gridData

                columnDefs: [
                    {
                    field: 'institution_name',
                    displayName: 'Institucion',
                    // width: '230'
                    },
                    {
                    field: 'unit_name',
                    displayName: 'Unidad Academica', 
                     // width: '380'
                    },
                    {
                    field: 'faculty_name',
                    displayName: 'Departamento',
                     // width: '310'
                    },
                    {
                    field: 'hasPermission',
                    displayName: 'Estado',
                    enableFiltering: false,
                    enableSorting: false,
                    // width: '90',
                    headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
                                        <input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="1" ng-false-value="0" ng-click="grid.appScope.groupsList.selectAllDepartament(checker.checked)" ng-disabled="(grid.appScope.edit == true)"> \
                                        <i class="green"></i></label></span></div>',
                    cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
                                    <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="1" ng-false-value="0" ng-disabled="(grid.appScope.edit == true)"> \
                                    <i class="green"></i></label></span></div>'

                    }
                ]
            };

            vm.init()

            function modalDelete(){
                    
                    $scope.userinfo = UserInfoConstant[0].details;
                    $scope.groupid = this.groupid;
                    $scope.groupname= this.groupname;
                    var modalInstance  = $modal.open({
                    template: require('../modal-delete/modalDeleteGroups.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'modalDeleteGroupsController',
                    controllerAs: 'modalDeleteGroups'

                });
            
            }

            function showGridCareer(){

                $scope.gridCampus = false;
                $scope.gridCareer = true;
                $scope.gridDepartament = false;
                
            }

            function showGridDepartament(){
                
                $scope.gridCampus = false;
                $scope.gridCareer = false;
                $scope.gridDepartament = true;       
            }

            function showGridCampus(){
                $scope.gridCampus = true;
                $scope.gridCareer = false;
                $scope.gridDepartament = false;
                
            }

            function editGroupsSave(){

                $scope.isDisabled = true;

                var arrayValues=[];
                angular.forEach(this.detailsGroups.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_campus_academicperiod_shift',section_id:value.id_section}) 
                    }
                });

                angular.forEach(this.detailsGroupsCareer.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_careers',section_id:value.id_section}) 
                    }
                });

                angular.forEach(this.detailsGroupsDepartament.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayValues.push({section_name:'upl_academicunits',section_id:value.id_section}) 
                    }
                });

                var values=arrayValues;

                let params  = { 

                    name   : $scope.name ,
                    user_id : vm.USER_ID,
                    values   : values 
                    
                }

                vm.saveGroupsRoles(params)
            }

    

   }//Fin constructor

   //functions
            
    loadGroups(item){

        this.params = {

            id:item.groupid
        }

        this.groupid=item.groupid;
        this.groupname=item.groupname;
        this.getDetailsGroupsFunction(this.params);
        this.getDetailsGroupsCareerFunction(this.params);
        this.getDetailsGroupsDepartamentFunction(this.params);
    }

    editGroups(){

        this.$scope.editSave = true;
        this.$scope.edit = false;
        this.$scope.titleList = false;
        this.$scope.titleEdit = true;
        this.$scope.isDisabled = false;
   
    }

    cancelEditGroups(){

        this.$scope.editSave = false;
        this.$scope.edit = true;
        this.$scope.titleList = true;
        this.$scope.titleEdit = false;
        this.getDetailsGroupsFunction({id : this.groupid});
        this.getDetailsGroupsCareerFunction({id : this.groupid});
        this.getDetailsGroupsDepartamentFunction({id : this.groupid});
   
    }

    selectAllCampus(value){
      
        this.detailsGroups.data.forEach(function(item){
                item.hasPermission=value;
        })
    }

    selectAllCareer(value){
      
        this.detailsGroupsCareer.data.forEach(function(item){
                item.hasPermission=value;
        })
    }
    selectAllDepartament(value){
      
        this.detailsGroupsDepartament.data.forEach(function(item){
                item.hasPermission=value;
        })
    }


    init(){

        this.services.getGroups().$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                 this.ngNotify.set('no existen registros','warn');

            }else{
                
                this.groups = dataReturn.data;
                this.groupid=this.groups[0].groupid;
                this.groupname=this.groups[0].groupname;
                this.params = {
                    id: this.groups[0].groupid
                }
                this.getDetailsGroupsFunction(this.params);
                this.getDetailsGroupsCareerFunction(this.params);
                this.getDetailsGroupsDepartamentFunction(this.params);
            }
        }, (err) => {
             this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })

    }

    getDetailsGroupsFunction(params){

        this.services.getAllPermissionsByCampusByGroup(params).$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');

            }else{
                
                this.detailsGroups.data = dataReturn.data;
                console.log('details groups',dataReturn.data);
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })

    }

    getDetailsGroupsCareerFunction(params){

        this.services.getAllPermissionsByCareerByGroup(params).$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');

            }else{
                
                this.detailsGroupsCareer.data = dataReturn.data;
                console.log('details groups',dataReturn.data);
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })

    }

    getDetailsGroupsDepartamentFunction(params){

        this.services.getAllPermissionsByDepartmentByGroup(params).$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');

            }else{

                this.detailsGroupsDepartament.data = dataReturn.data;
                console.log('details groups',dataReturn.data);
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })

    }

    saveGroupsRoles(params){

        console.log('esto es lo que se envia',params);

            let updateGroups = {
                     groupid: this.groupid,
                     values : params.values
                    }

            this.services.saveGroupsPermissions(updateGroups).$promise.then((dataReturn) => {
                if(dataReturn.status == 0){
                    this.ngNotify.set('se ha modificado de manera correcta el Grupo','success');
                    // this.$state.go(this.$state.current, {}, {reload: true});
                    this.$scope.edit = true;
                    this.$scope.editSave = false;
                    this.$scope.titleList = true;
                    this.$scope.titleEdit = false;

                }else{
                    console.log('Error Servicio update check params enviados',dataRoles);
                    this.$scope.isDisabled = false;
                }
            }, (err) => {
                this.ngNotify.set('No se ha podido conectar con el servicio','error');
                this.$scope.isDisabled = false;
            })

    }


}// fin function principal
groupsListController.$inject=['$scope', 'groupsServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location']





