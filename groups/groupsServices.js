class groupsServices {

    constructor($resource,SERVICE_URL_CONSTANT) {

       
          var services = $resource(SERVICE_URL_CONSTANT.adminPanel +'/user-api/:route',{},{
          // var services = $resource(SERVICE_URL_CONSTANT.localJson +'/admin-panel/:routeJson',{},{
            
            getGroups:{
              method:'GET',
              params : {
                route: 'getGroups'
                // routeJson: 'getGroups.json'
              }
            },

            getAllPermissionsByCampusByGroup:{
              method:'GET',
              params : {
                route: 'getAllPermissionsByCampusByGroup'
                // routeJson: 'getAllPermissionsByCampusByGroup.json'
              }
            },

            getAllPermissionsByCareerByGroup:{
              method:'GET',
              params : {
                route: 'getAllPermissionsByCareerByGroup'
                // routeJson: 'getAllPermissionsByCareerByGroup.json'
              }
            },

            getAllPermissionsByDepartmentByGroup:{
              method:'GET',
              params : {
                route: 'getAllPermissionsByDepartmentByGroup'
                // routeJson: 'getAllPermissionsByDepartmentByGroup.json'
              }
            },

            saveGroups:{
              method:'POST',
              params : {
                route: 'insertGroups'
                // routeJson: 'insertGroups.json'
              }
            },

            saveGroupsPermissions:{
              method:'POST',
              params : {
                route: 'updatesectiongroup'
              }
            },

            deleteGroups:{
              method:'POST',
              params : {
                route: 'deactiveGroups'
              }
            }

          });

          return {
            services : services
          };
        }

    }

  groupsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.groupsServices', [])
  .service('groupsServices', groupsServices)
  .name;
